/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.human;

/**
 *
 * @author ทักช์ติโชค
 */
public class Human {

    String name;  // ชื่อ
    int age;  // อายุ
    char sex; // เพศ

    void walk() {  // method เดินเล่ม
        System.out.println("Name: " + this.name + " Age: " + this.age + " Sex: " + this.sex + " Today I walk");  
        //แสดงชื่อ อายุ เพศ ที่ถูกเรียก ตัวmethod  ว่าวันนี้ฉันเดิน 
    }

    void eat() {  // method กิน
        System.out.println("Name: " + this.name + " Age: " + this.age + " Sex: " + this.sex + " Today I eat");
        //แสดงชื่อ อายุ เพศ ที่ถูกเรียก ตัวmethod  ว่าวันนี้ฉันกิน 
    }

    void sleep() { // method นอน
        System.out.println("Name: " + this.name + " Age: " + this.age + " Sex: " + this.sex + " Today I sleep");
        //แสดงชื่อ อายุ เพศ ที่ถูกเรียก ตัวmethod  ว่าวันนี้ฉันนอน 
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public char getSex() {
        return sex;
    }

}
