/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.human;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestHuman {

    public static void main(String[] args) {
        Human human1 = new Human(); // สร้าง Object
        Human human2 = new Human(); // สร้าง Object
        human1.name = "Lucy"; // คนที่1 ชื่อ ลูซี่
        human1.age = 18; // คนที่1 อายุ 18 ปี
        human1.sex = 'F'; // คนที่1 เพศ หญิง
        human2.name = "Ball"; // คนที่2 ชื่อ บอล
        human2.age = 20; // คนที่2 อายุ 20 ปี
        human2.sex = 'M'; // คนที่2 เพศ ชาย
        
        human1.walk(); // คนที่1 เรียกไปยังที่เลือก method เดิม
        human2.eat(); //คนที่2 เรีกกไปยังที่เลือก method กิน
        human2.walk(); //คนที่2 เรีกกไปยังที่เลือก method เดิม
        human1.sleep(); //คนที่1 เรียกไปยังที่เลือก method นอน
        
    }
}
